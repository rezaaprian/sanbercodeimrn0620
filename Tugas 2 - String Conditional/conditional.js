//Soal 1
console.log('-- Soal 1 (if else)');
var nama = "John";
var peran = "Raja Terakhir";

//case sensitive
if (nama == '') {
    console.log('Nama harus diisi!');
} else if (peran == '') {
    console.log('Halo ' + peran + ', Pilih peranmu untuk memulai game!');
} else {
    console.log('Selamat datang di Dunia Werewolf, ' + nama);
    if (peran == 'Penyihir') {
        console.log('Halo ' + peran + ' ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf.');
    } else if (peran == 'Guard') {
        console.log('Halo ' + peran + ' ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.');
    } else if (peran == 'Werewolf') {
        console.log('Halo ' + peran + ' ' + nama + ', Kamu akan memakan mangsa setiap malam!');
    } else {
        console.log('Peran yang kamu pilih tidak tersedia di game Werewolf!');
    }
}

//Soal 2
console.log('\n-- Soal 2 (switch case)');
var hari = 16;
var bulan = 6;
var tahun = 2020;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
var bulanInCapital;

//skeleton check
if ((hari > 0 && hari <= 31) && (bulan > 0 && bulan <= 12) && (tahun > 1900 && tahun <= 2200)) {
    switch (bulan) {
        case 1:
            bulanInCapital = 'Januari';
            break;
        case 2:
            bulanInCapital = 'Febuari';
            break;
        case 3:
            bulanInCapital = 'Maret';
            break;
        case 4:
            bulanInCapital = 'April';
            break;
        case 5:
            bulanInCapital = 'Mei';
            break;
        case 6:
            bulanInCapital = 'Juni';
            break;
        case 7:
            bulanInCapital = 'Juli';
            break;
        case 8:
            bulanInCapital = 'Agustus';
            break;
        case 9:
            bulanInCapital = 'September';
            break;
        case 10:
            bulanInCapital = 'Oktober';
            break;
        case 11:
            bulanInCapital = 'November';
            break;
        case 12:
            bulanInCapital = 'Desember';
            break;
    }
    console.log(hari + ' ' + bulanInCapital + ' ' + tahun);
} else {
    console.log('format input salah');
}

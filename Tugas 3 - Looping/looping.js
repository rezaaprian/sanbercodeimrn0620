//Soal 1
console.log('-- Soal 1 (Looping while)');
var count1 = 2;
console.log('LOOPING PERTAMA');
while (count1 <= 20) {
    console.log(count1 + ' - I love coding');
    count1 += 2;
}
count1 -= 2;
console.log('LOOPING KEDUA');
while (count1 >= 2) {
    console.log(count1 + ' - I will become a mobile developer');
    count1 -= 2;
}


//Soal 2
console.log('\n-- Soal 2 (Looping for)');
for (var count2 = 1; count2 <= 20; count2++) {
    if (count2 % 3 == 0 && count2 % 2 != 0)
        console.log(count2 + ' - I Love Coding');
    else if (count2 % 2 == 0)
        console.log(count2 + ' - Berkualitas');
    else
        console.log(count2 + ' - Santai');
}

//Soal 3
console.log('\n-- Soal 3 (Persegi Panjang)');
var stringBuilder = '';
for (var i = 0; i < 4; i++) {
    if (i > 0)
        stringBuilder += '\n';
    for (var j = 0; j < 8; j++) {
        stringBuilder += '#';
    }
}
console.log(stringBuilder)

//Soal 4
console.log('\n-- Soal 4 (Tangga)');
var stringBuilder = '';
for (var i = 0; i < 7; i++) {
    if (i > 0)
        stringBuilder += '\n';
    for (var j = 0; j < i + 1; j++) {
        stringBuilder += '#';
    }

}
console.log(stringBuilder)

//Soal 5
console.log('\n-- Soal 5 (Papan Catur)');
stringBuilder = '';
for (var i = 0; i < 8; i++) {
    if (i > 0)
        stringBuilder += '\n'
    for (var j = 0; j < 8; j++) {
        if (j % 2 == 0) {
            if (i % 2 == 0)
                stringBuilder += ' '
            else
                stringBuilder += '#'
        } else {
            if (i % 2 == 0)
                stringBuilder += '#'
            else
                stringBuilder += ' '
        }
    }
}
console.log(stringBuilder)

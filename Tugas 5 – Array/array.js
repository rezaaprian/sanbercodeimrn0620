//Soal 1
console.log('-- Soal 1 (Range)');
function range(start, end) {
    var arr = [];
    if (start != null && end != null) {
        if (start <= end) {
            for (var i = start; i <= end; i++) {
                arr.push(i);
            }
        } else {
            for (var i = start; i >= end; i--) {
                arr.push(i); //or use unshift with reverse looping
            }
        }
    } else {
        return -1;
    }
    return arr;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//Soal 2
console.log('\n-- Soal 2 (Range with step)');
function rangeWithStep(start = 0, end = 0, step = 1) {
    var arr = [];
    if (start != null && end != null && step != null) {
        var i = start;
        if (start <= end) {
            while (i <= end) {
                arr.push(i);
                i += step;
            }
        } else {
            while (i >= end) {
                arr.push(i);
                i -= step;
            }
        }
    } else {
        return -1;
    }
    return arr;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//Soal 3
console.log('\n-- Soal 3 (Sum of range)');
function sum(start, end, step) {
    var arr = rangeWithStep(start, end, step);
    var sum = 0;
    for (var i in arr) {
        sum += arr[i];
    }
    return sum;
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//Soal 4
console.log('\n-- Soal 4 (Array Multidimensi)');
function dataHandling(datas) {
    for (var i in datas) {
        console.log("Nomor ID: " + datas[i][0]);
        console.log("Nama Lengkap: " + datas[i][1]);
        console.log("TTL: " + datas[i][2] + " " + datas[i][3]);
        console.log("Hobi: " + datas[i][4] + "\n");
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input);

//Soal 5
console.log('\n-- Soal 5 (Balik Kata)');
function balikKata(sentence) {
    var output = "";
    for (var i = sentence.length - 1; i >= 0; i--) {
        output += sentence[i];
    }
    return output;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//Soal 6
console.log('\n-- Soal 6 (Metode Array)');
function getMonthInCapital(month) {
    switch (month) {
        case "01": return "Januari";
        case "02": return "Febuari";
        case "03": return "Maret";
        case "04": return "April";
        case "05": return "Mei";
        case "06": return "Juni";
        case "07": return "Juli";
        case "08": return "Agustus";
        case "09": return "September";
        case "10": return "Oktober";
        case "11": return "November";
        case "12": return "Desember";
    }
}

function dataHandling2(input) {
    input.splice(1, 1, input[1] + "Elsharawy");
    input.splice(2, 1, "Provinsi " + input[2]);
    input.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(input);

    var dateTime = input[3].split("/");
    console.log(getMonthInCapital(dateTime[1]));
    console.log(dateTime.slice().sort(function (a, b) { return b-a }));
    console.log(dateTime.join("-"));
    console.log(input[1].slice(0, 15));
}


var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
